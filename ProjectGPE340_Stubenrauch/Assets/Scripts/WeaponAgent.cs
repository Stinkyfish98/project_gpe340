﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponAgent : MonoBehaviour
{
    protected Animator animator;
    [HideInInspector]
    public Weapon equippedWeapon = null;
    [Header("Weapon Settings")]
    public Transform attachmentPoint;

    public void EquipWeapon(Weapon weapon)
    {
        equippedWeapon = Instantiate(weapon) as Weapon;
        animator.SetInteger("Weapon Animation Type", weapon.getEnum());
        equippedWeapon.transform.SetParent(attachmentPoint);
        equippedWeapon.transform.localPosition = weapon.transform.localPosition;
        equippedWeapon.transform.localRotation = weapon.transform.localRotation;
        equippedWeapon.gameObject.layer = gameObject.layer;
    }

    public void Unequip()
    {
        if (equippedWeapon)
        {
            Destroy(equippedWeapon.gameObject);
            equippedWeapon = null;
        }
    }

    protected virtual void OnAnimatorIK()
    {
        if (!equippedWeapon)
            return;
        if (equippedWeapon.RightHandIKTarget)
        {
            animator.SetIKPosition(AvatarIKGoal.RightHand, equippedWeapon.RightHandIKTarget.position);
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
            animator.SetIKRotation(AvatarIKGoal.RightHand, equippedWeapon.RightHandIKTarget.rotation);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
        }
        else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);
        }
        if (equippedWeapon.LeftHandIKTarget)
        {
            animator.SetIKPosition(AvatarIKGoal.LeftHand, equippedWeapon.LeftHandIKTarget.position);
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
            animator.SetIKRotation(AvatarIKGoal.LeftHand, equippedWeapon.LeftHandIKTarget.rotation);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
        }
        else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0f);
        }
    }

}
