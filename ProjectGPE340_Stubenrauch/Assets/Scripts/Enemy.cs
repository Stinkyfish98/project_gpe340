﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class Enemy : WeaponAgent
{

    private NavMeshAgent navMeshAgent;
    public Transform enemyHealthBarContainer;

    [Header("Enemy Settings")]
    public float speed;
    public Weapon[] defaultWeapons;

    [Header("Item Drop Settings")]
    public WeightedObject[] itemDrops;
    [Range(0.0f, 1.0f)]
    public float itemDropChance;
    public Vector3 itemDropOffset;

    // Start is called before the first frame update
    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        EquipWeapon(defaultWeapons[Random.Range(0, defaultWeapons.Length)]);
        UIManager.Instance.RegisterEnemy(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.paused)
        {
            return;
        }

        if(!GameManager.spawnedPlayer)
        {
            navMeshAgent.Stop();
            animator.SetFloat("Horizontal", 0f);
            animator.SetFloat("Vertical", 0f);
            return;
        }

        navMeshAgent.SetDestination(GameManager.spawnedPlayer.transform.position);
        Vector3 input = navMeshAgent.desiredVelocity;
        input = transform.InverseTransformDirection(input);
        animator.SetFloat("Horizontal", input.x);
        animator.SetFloat("Vertical", input.z);

        Vector3 playerDir = GameManager.spawnedPlayer.transform.position - transform.position;
        float distance = Vector3.Distance(GameManager.spawnedPlayer.transform.position, transform.position);
        float angle = Vector3.Angle(playerDir, transform.forward);

        if(angle < equippedWeapon.attackAngle &&  distance < equippedWeapon.range)
        {
            equippedWeapon.PullTrigger();
        }
        else
        {
            equippedWeapon.ReleaseTrigger();
        }

    }

    private void OnAnimatorMove()
    {
        navMeshAgent.velocity = animator.velocity;
    }

    public void DropItem()
    {
        if (Random.Range(0f, 1f) < itemDropChance)
            Instantiate(WeightedObject.Select(itemDrops), transform.position + itemDropOffset, Quaternion.identity);
    }
}

[System.Serializable]
public class WeightedObject
{
    [SerializeField, Tooltip("The object selected by this choice.")]
    private Object value;
    [SerializeField, Tooltip("The chance to select the value.")]
    private double chance = 1.0;

    public static Object Select(WeightedObject[] choices)
    {
        double[] cdfArray = new double[choices.Length];
        System.Random rnd = new System.Random();

        for(int i = 0; choices.Length - 1 >= i; i++)
        {
            for(int j = 0; i >= j; j++)
            {
                cdfArray[i] += choices[j].chance;
            }
        }

        int selectedIndex = System.Array.BinarySearch(cdfArray, rnd.NextDouble() * cdfArray.Last());
        if (selectedIndex < 0)
            selectedIndex = ~selectedIndex;
        return choices[selectedIndex].value;
    }
}
