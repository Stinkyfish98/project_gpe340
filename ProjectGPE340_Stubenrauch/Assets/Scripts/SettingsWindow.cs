﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsWindow : MonoBehaviour
{

    [Header("Audio Settings")]
    [SerializeField]
    private AudioMixer audioMixer;
    [SerializeField]
    private AnimationCurve volumeVsDecibels;
    public Slider masterVolume;
    public Slider soundVolume;
    public Slider musicVolume;
    [Header("Graphics Settings")]
    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;
    public Toggle fullscreen;
    public Button applyButton;
    int currentResolutionIndex = 0;
    Resolution[] resolutions;

    // Start is called before the first frame update
    void Awake()
    {
        // Build resolutions
        resolutionDropdown.ClearOptions();

        resolutions = Screen.resolutions;
        List<string> options = new List<string>();
        for (int index = 0; index < resolutions.Length; index++)
        {
            string option = string.Format("{0} x {1}", resolutions[index].width, resolutions[index].height);
            options.Add(option);

            if (resolutions[index].width == Screen.currentResolution.width && resolutions[index].height == Screen.currentResolution.height)
                currentResolutionIndex = index;
        }
        resolutionDropdown.AddOptions(options);
        // Build quality levels
        qualityDropdown.ClearOptions();
        qualityDropdown.AddOptions(QualitySettings.names.ToList());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        masterVolume.value = PlayerPrefs.GetFloat("Master Volume", masterVolume.value);
        musicVolume.value = PlayerPrefs.GetFloat("Music Volume", musicVolume.value);
        soundVolume.value = PlayerPrefs.GetFloat("Music Volume", soundVolume.value);
        Debug.Log(Screen.fullScreen);
        fullscreen.isOn = Screen.fullScreen;
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        applyButton.interactable = false;
    }

    public void SetQuality()
    {
        QualitySettings.SetQualityLevel(qualityDropdown.value);
    }

    public void SetResolution()
    {
        Resolution resolution = resolutions[resolutionDropdown.value];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetFullscreen()
    {
        Screen.fullScreen = fullscreen.isOn;
        Debug.Log(fullscreen.isOn);
    }

    private void SetMasterVolume()
    {
        audioMixer.SetFloat("Master Volume", volumeVsDecibels.Evaluate(masterVolume.value));

    }

    private void SetMusicVolume()
    {
        audioMixer.SetFloat("Music Volume", volumeVsDecibels.Evaluate(musicVolume.value));
    }

    private void SetSoundVolume()
    {
        audioMixer.SetFloat("Sound Volume", volumeVsDecibels.Evaluate(soundVolume.value));
    }

    public void OnApply()
    {
        SetQuality();
        SetResolution();
        SetFullscreen();
        SetMasterVolume();
        SetMusicVolume();
        SetSoundVolume();
    }

}
