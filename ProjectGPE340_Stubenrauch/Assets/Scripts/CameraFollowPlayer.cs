﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {

    public float smooth = 0.3f;

    public float height;

    private Vector3 velocity = Vector3.zero;
	
	// Update is called once per frame
	void Update () {

        Vector3 pos = new Vector3();
        pos.x = GameManager.spawnedPlayer.transform.position.x;
        pos.z = GameManager.spawnedPlayer.transform.position.z;
        pos.y = GameManager.spawnedPlayer.transform.position.y + height;
        transform.position = Vector3.SmoothDamp(transform.position, pos, ref velocity, smooth);
        
	}
}
