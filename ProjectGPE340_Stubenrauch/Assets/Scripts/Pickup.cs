﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour {

    private Vector3 axis = Vector3.up;
    public float rotationSpeed = 90f;
    [SerializeField]
    private float lifespan;

    // Use this for initialization
    void Awake () {
        Destroy(gameObject, lifespan);
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation *= Quaternion.AngleAxis(rotationSpeed * Time.deltaTime, axis);
    }

    private void OnTriggerEnter(Collider collider)
    {
        Player player = collider.GetComponent<Player> ();
        if (player)
        {
            OnPickUp(player);
        }
    }

    protected virtual void OnPickUp(Player player)
    {
        Destroy(gameObject);
    }

}
