﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{

    [SerializeField]
    private float initialHealth;
    [SerializeField]
    private float maxHealth;
    [Header("Events")]
    [SerializeField, Tooltip("Raised every time the object is healed.")]
    private UnityEvent onHeal;
    [SerializeField, Tooltip("Raised every time the object is damaged.")]
    private UnityEvent onDamage;
    [SerializeField, Tooltip("Raised once when the object's health reaches 0.")]
    public UnityEvent onDie;

    public float health
    {
        get
        {
            return this.initialHealth;
        }

        private set
        {
            this.initialHealth = value;
        }
    }


    public void Damage(float damage)
    {
        damage = Mathf.Max(damage, 0f);
        initialHealth = Mathf.Clamp(initialHealth - damage, 0f, maxHealth);
        SendMessage("OnDamage", SendMessageOptions.DontRequireReceiver);
        onDamage.Invoke();
        if (initialHealth == 0f)
        {
            SendMessage("OnDie", SendMessageOptions.DontRequireReceiver);
            onDie.Invoke();
        }
    }
    
    public void Heal(float heal)
    {
        initialHealth = Mathf.Clamp(initialHealth + heal, 0f, maxHealth);
        SendMessage("OnHeal", SendMessageOptions.DontRequireReceiver);
        onHeal.Invoke();
    }
    
    public float Percent()
    {
        float percent = initialHealth / maxHealth;
        percent *= 100f;
        Mathf.RoundToInt(percent);
        return percent;
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

}
