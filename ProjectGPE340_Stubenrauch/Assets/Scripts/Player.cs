﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Player : WeaponAgent {

    [Header("Player Settings")]
    [SerializeField, Tooltip("The max speed of the player")]
    private float speed = 4f;
    public Weapon defaultWeapon;

    public Health health { get; private set; }

    private void Awake()
    {
        animator = GetComponent<Animator>();
        health = GetComponent<Health>();
        UIManager.Instance.RegisterPlayer(this);
    }

    private void Update()
    {
        if(GameManager.paused)
        {
            return;
        }

        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        input = Vector3.ClampMagnitude(input, 1f);
        input = transform.InverseTransformDirection(input);
        input *= speed;
        animator.SetFloat("Horizontal", input.x);
        animator.SetFloat("Vertical", input.z);
        if (Input.GetKeyDown(KeyCode.LeftControl) == true)
        {
            if(animator.GetBool("Crouched") == true)
            {
                animator.SetBool("Crouched", false);
            }
            else
                animator.SetBool("Crouched", true);
        }
        if (Input.GetKeyDown(KeyCode.Q) == true)
        {
            EquipWeapon(defaultWeapon);
        }
        if (equippedWeapon != null)
        {
            if (Input.GetButton("Fire1"))
            {
                equippedWeapon.PullTrigger();
            }
            else
                equippedWeapon.ReleaseTrigger();
        }
    }
}
