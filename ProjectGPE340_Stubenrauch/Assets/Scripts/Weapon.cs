﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public enum WeaponAnimationType
    {
        None = 0,
        Rifle = 1,
        Handgun = 2
    }

    [Header("Weapon Setting")]
    [SerializeField]
    private WeaponAnimationType animationType = WeaponAnimationType.None;
    public float damage;
    public float range;
    public float attackAngle;
    public Sprite icon;

    [Header("IK Settings")]
    public Transform RightHandIKTarget;
    public Transform LeftHandIKTarget;

    public int getEnum()
    {
        return (int) animationType;
    }

    public abstract void PullTrigger();
    public abstract void ReleaseTrigger();

}
