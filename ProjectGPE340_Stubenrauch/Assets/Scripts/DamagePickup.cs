﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePickup : Pickup
{
    [SerializeField, Tooltip("The amount of damage to apply to the player")]
    private float damage;

    protected override void OnPickUp(Player player)
    {
        player.health.Damage(damage);
        base.OnPickUp(player);
    }
}
