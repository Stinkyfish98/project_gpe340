﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RespawnableEnemy : MonoBehaviour
{

    public Enemy enemy;
    private int currentActiveEnemies = 0;
    public int maxActiveEnemies;
    public float spawnDelay;
    private int spawnedEnemyCount = 0;
    public int enemyCount;
    [SerializeField]
    private UnityEvent onEmpty;

    private void Start()
    {
        InvokeRepeating("SpawnEnemy", 0f, spawnDelay);
        GameManager.enemySpawnerCount += 1;
        this.onEmpty.AddListener(HandleSpawnerEmpty);
    }

    private void Update()
    {
        if(spawnedEnemyCount >= enemyCount)
        {
            CancelInvoke("SpawnEnemy");
            if (currentActiveEnemies <= 0)
            {
                onEmpty.Invoke();
            }
        }
    }

    private void SpawnEnemy()
    {
        if (currentActiveEnemies >= maxActiveEnemies)
            return;

        Enemy newEnemy = Instantiate(enemy, transform.position, transform.rotation) as Enemy;
        Health health = newEnemy.GetComponent<Health>();
        currentActiveEnemies++;
        spawnedEnemyCount++;
        health.onDie.AddListener(HandleEnemyDeath);
    }

    private void HandleEnemyDeath()
    {
        currentActiveEnemies--;
    }

    private void HandleSpawnerEmpty()
    {
        GameManager.enemySpawnerCount--;
    }

}
