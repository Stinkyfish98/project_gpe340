﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Image fill;
    [SerializeField]
    private Text text;
    [SerializeField]
    private string textFormat;
    [SerializeField]
    private bool destroyWithTarget;
    [SerializeField]
    private bool trackTarget;
    [SerializeField]
    private Vector3 trackingOffset;
    [HideInInspector]
    public Health target;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (target.GetComponent<Player>())
            TextFormat();

        Fill();

        if (trackTarget == true)
            TrackTarget();

        if (destroyWithTarget == true)
            DestroyWithTarget();
    }

    public void Fill()
    {
        fill.fillAmount = target.health / 100;

    }

    public void TextFormat()
    {
        text.text = string.Format(textFormat, Mathf.RoundToInt(target.Percent()));

    }

    public void SetTarget(Health health)
    {
        target = health;
    }

    private void TrackTarget()
    {
        Vector3 trackingLocation = target.GetComponent<Transform>().position;
        trackingLocation += trackingOffset;
        this.transform.position = trackingLocation;
    }

    private void DestroyWithTarget()
    {
        if (target.isActiveAndEnabled != true)
        {
            Destroy(gameObject);
        }
    }

}
