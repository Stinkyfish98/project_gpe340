﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : Pickup
{
    [SerializeField, Tooltip("The amount of healing to apply to the player")]
    private float healing;

    protected override void OnPickUp (Player player)
    {
        player.health.Heal(healing);
        base.OnPickUp(player);
    }
    
}
