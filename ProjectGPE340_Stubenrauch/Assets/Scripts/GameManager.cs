﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }

    [Header("Level Settings")]
    public Player playerPrefab;
    public Transform playerSpawnPoint;
    public float playerRespawnDelay;
    [HideInInspector]
    public static int enemySpawnerCount;
    [HideInInspector]
    public static Player spawnedPlayer;
    [HideInInspector]
    public static bool paused = false;
    public static int playerLives = 3;
    [SerializeField]
    private UnityEvent onPause;
    [SerializeField]
    private UnityEvent onResume;
    [SerializeField]
    private UnityEvent onLose;
    [SerializeField]
    private UnityEvent onWin;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        SpawnPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            Pause();
        }

        if(enemySpawnerCount <= 0)
        {
            onWin.Invoke();
        }
    }

    private void SpawnPlayer()
    {
        spawnedPlayer = Instantiate(playerPrefab, playerSpawnPoint.position, playerSpawnPoint.rotation);

        spawnedPlayer.health.onDie.AddListener (HandlePlayerDeath);
    }

    private void HandlePlayerDeath()
    {
        spawnedPlayer.health.onDie.RemoveListener(HandlePlayerDeath);
        playerLives -= 1;
        if (playerLives == 0)
        {
            onLose.Invoke();
            return;
        }
        Invoke("SpawnPlayer", playerRespawnDelay);
    }

    public static void Pause()
    {
        paused = true;
        Time.timeScale = 0f;
        Instance.onPause.Invoke();
    }
        
    public static void Resume()
    {
        paused = false;
        Time.timeScale = 1f;
        Instance.onResume.Invoke();
    }



}
