﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public HealthBar playerHealthBar;
    public HealthBar enemyHealthBarPrefab;
    public Transform enemyHealthBarContainer;
    public Image weaponDisplay;
    public Text lifeDisplay;
    public string lifeDisplayFormat;
    public GameObject pauseMenu;
    public GameObject loseMenu;
    public GameObject winMenu;
    public static UIManager Instance { get; private set; }
    public GameObject settingsMenu;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.spawnedPlayer)
        {
            if (GameManager.spawnedPlayer.equippedWeapon)
            {
                weaponDisplay.overrideSprite = GameManager.spawnedPlayer.equippedWeapon.icon;
            }
            lifeDisplay.text = string.Format(lifeDisplayFormat, GameManager.playerLives);
        }
    }

    public void RegisterPlayer(Player player)
    {
        playerHealthBar.SetTarget(player.health);
    }

    public void RegisterEnemy(Enemy enemy)
    {
        HealthBar healthBar = Instantiate(enemyHealthBarPrefab) as HealthBar;
        healthBar.transform.SetParent(enemyHealthBarContainer, false);
        healthBar.SetTarget(enemy.GetComponent<Health>());
    }

    public void ShowPauseMenu()
    {
        pauseMenu.SetActive(true);
    }

    public void HidePauseMenu()
    {
        pauseMenu.SetActive(false);
    }

    public void ShowLoseMenu()
    {
        loseMenu.SetActive(true);
    }

    public void ShowWinMenu()
    {
        winMenu.SetActive(true);
    }

    public void ButtomResume()
    {
        GameManager.Resume();
    }

    public void ButtonQuit()
    {
        Application.Quit();
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }

    public void ShowSettingsMenu()
    {
        settingsMenu.SetActive(true);
    }

    public void HideSettingsMenu()
    {
        settingsMenu.SetActive(false);
    }
}
