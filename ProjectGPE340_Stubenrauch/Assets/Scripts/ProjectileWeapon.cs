﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeapon : Weapon
{

    public bool triggerPulled;
    [Header("Projectile Settings")]
    public Projectile projectilePrefab;
    public Transform barrel;
    public float muzzleVelocity;
    public float shotsPerMinute;
    private float timeNextShotIsReady;
    public float spread = 15;
    [SerializeField]
    private AudioSource barrelAudioSource;
    [SerializeField]
    private AudioClip weaponSound;
    [SerializeField]
    private ParticleSystem muzzleFlashParticle;

    private void Awake()
    {
        timeNextShotIsReady = Time.time;
    }

    private void Update()
    {
        if(triggerPulled)
        {
            while (Time.time > timeNextShotIsReady)
            {
                if (barrelAudioSource)
                    barrelAudioSource.PlayOneShot(weaponSound);
                if (muzzleFlashParticle)
                    muzzleFlashParticle.Emit(1);
                Projectile projectile = Instantiate(projectilePrefab, barrel.position, barrel.rotation * Quaternion.Euler(1, (Random.onUnitSphere.z * spread), 1)) as Projectile;
                projectile.damage = damage;
                projectile.gameObject.layer = gameObject.layer;
                projectile.rigidbody.AddRelativeForce(Vector3.forward * muzzleVelocity, ForceMode.VelocityChange);
                timeNextShotIsReady += 60f / shotsPerMinute;
            }
        }
        else if (Time.time > timeNextShotIsReady)
        {
            timeNextShotIsReady = Time.time;
        }

    }

    public override void PullTrigger()
    {
        triggerPulled = true;
    }

    public override void ReleaseTrigger()
    {
        triggerPulled = false;
    }
}
