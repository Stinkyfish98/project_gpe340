﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [HideInInspector]
    public float damage;
    private int lifespan = 4;
    [HideInInspector]
    public Rigidbody rigidbody;


    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, lifespan);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collider)
    {
        Health health = collider.GetComponent<Health>();
        if (health)
        {
            health.Damage(damage);
        }
        Destroy(gameObject);
    }
}
